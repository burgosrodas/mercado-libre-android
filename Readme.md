# Mercado Libre Challenge
The purpose of this app is to display the products from the MeLi's API allowing search, select and display info related to the product.

![mercadoLibreImage](app/src/main/res/mipmap-xxxhdpi/ic_launcher_round.png)

### Tools:
1. [Android Studio](https://developer.android.com/studio/)
2. [Kotlin](https://developer.android.com/kotlin)
3. [Mercado Libre API](https://developers.mercadolibre.com.ar/es_ar/items-y-busquedas)
4. [Git](https://git-scm.com/)
5. [Bitbucket](https://bitbucket.org/)
6. [Postman](https://www.postman.com/)

### Architectural Components & External Libraries
* [Live Data](https://developer.android.com/topic/libraries/architecture/livedata) - Lifecycle-aware data holders.
* [View Model](https://developer.android.com/topic/libraries/architecture/viewmodel) - Lifecycle-aware VM implementation.
* [Paging](https://developer.android.com/topic/libraries/architecture/paging) - Load large sets of data gradually.
* [Navigation](https://developer.android.com/guide/navigation/navigation-getting-started) - Navigating between Fragments.
* [ViewBinding](https://developer.android.com/topic/libraries/view-binding) - Used to connect layout ids to Views.
* [Dagger Hilt](https://dagger.dev/hilt/) - Used for dependency injection.
* [Material Components](https://github.com/material-components/material-components-android) - UI components from Google.
* [Retrofit](https://square.github.io/retrofit/) - A type-safe HTTP client.
* [Room](https://developer.android.com/jetpack/androidx/releases/room) - SQLite database management.
* [OkHttp](https://square.github.io/okhttp/) - Low-level networking.
* [Picasso](https://square.github.io/picasso/) - For image downloading and caching.
* [Coroutines](https://developer.android.com/kotlin/coroutines-adv) - Asynchronous programming, reactive programming.

### Conventions
* [Conventional Commits 1.0.0](https://www.conventionalcommits.org/en/v1.0.0/) - A specification for adding human and machine readable meaning to commit messages.

### Branches

* **[main](https://bitbucket.org/burgosrodas/mercado-libre-android/src/main/)** *(Production)*
* **[develop](https://bitbucket.org/burgosrodas/mercado-libre-android/src/develop/)** *(Development)*
* **[feature/pagination](https://bitbucket.org/burgosrodas/mercado-libre-android/src/ea8867f2531457b8d099e35a382b158097f19436/?at=feature%2Fpagination)** *(feature)*

### Architecture

* MVVM

![MVVM](https://miro.medium.com/max/1400/1*hiypGQVOat8W3411_SUaTg.jpeg)

## Preview

### Light Mode
![Search-LightMode](captures/Search-LightMode.png)
![SearchField-LightMode](captures/SearchField-LightMode.png)
![SearchResults-LightMode](captures/SearchResults-LightMode.png)
![ProductDetail-LightMode](captures/ProductDetail-LightMode.png)

### Dark Mode
![Search-DarkMode](captures/Search-DarkMode.png)
![SearchField-DarkMode](captures/SearchField-DarkMode.png)
![SearchResults-DarkMode](captures/SearchResults-DarkMode.png)
![ProductDetail-DarkMode](captures/ProductDetail-DarkMode.png)
