package com.challenge.mercadolibre

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MercadoLibreApplication: Application()