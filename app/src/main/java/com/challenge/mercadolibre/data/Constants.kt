package com.challenge.mercadolibre.data

const val API_BASE_URL = "https://api.mercadolibre.com/"
const val API_SITE_ID = "MLA"
