package com.challenge.mercadolibre.data.networking

import com.challenge.mercadolibre.com.challenge.mercadolibre.data.model.response.ProductDescriptionResponse
import com.challenge.mercadolibre.data.model.response.ProductItemResponse
import com.challenge.mercadolibre.data.API_SITE_ID
import com.challenge.mercadolibre.data.model.response.ProductSearchResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MercadoLibreAPI {

    @GET("/sites/${API_SITE_ID}/search")
    suspend fun search(@Query("q") query: String): ProductSearchResponse

    @GET("/items/{itemId}")
    suspend fun getItemById(@Path("itemId") itemId: String): ProductItemResponse

    @GET("/items/{itemId}/description")
    suspend fun getItemDescriptionById(@Path("itemId") itemId: String): ProductDescriptionResponse
}
