package com.challenge.mercadolibre.com.challenge.mercadolibre.data.model.response

import com.google.gson.annotations.SerializedName

data class ProductDescriptionResponse(
    val text: String,
    @SerializedName("plain_text")
    val plainText: String
)
