package com.challenge.mercadolibre.data.model.response

import com.google.gson.annotations.SerializedName

data class ProductItemResponse(
    val id: String,
    val title: String,
    val price: Float,
    @SerializedName("currency_id")
    val currencyId: String,
    @SerializedName("available_quantity")
    val quantity: String,
    val condition: String,
    @SerializedName("secure_thumbnail")
    val thumbnail: String,
    val warranty: String,
    val pictures: List<Picture>
)

data class Picture(
    val id: String,
    @SerializedName("secure_url")
    val url: String
)
