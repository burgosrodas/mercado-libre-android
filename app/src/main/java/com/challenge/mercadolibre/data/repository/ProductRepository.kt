package com.challenge.mercadolibre.data.repository

import com.challenge.mercadolibre.common.handleError
import com.challenge.mercadolibre.data.networking.MercadoLibreAPI
import javax.inject.Inject

class ProductRepository @Inject constructor(
    private val apiService: MercadoLibreAPI
) {

    /**
     * Gets all products as a result of the search.
     *
     * @param query word to be searched.
     *
     * @return [Product] list.
     * */
    suspend fun search(query: String) = handleError {
        apiService.search(query = query)
    }

    /**
     * Gets a product by a given Id.
     *
     * @param productId String with the product id to be retrieved.
     *
     * @return [Product].
     * */
    suspend fun getProductById(productId: String) = handleError {
        apiService.getItemById(productId)
    }

    /**
     * Gets a product's description by a given Id.
     *
     * @param productId String with the product id to be retrieved.
     *
     * @return [ProductDescription].
     * */
    suspend fun getProductDescriptionById(productId: String) = handleError {
        apiService.getItemDescriptionById(productId)
    }
}
