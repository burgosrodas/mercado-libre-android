package com.challenge.mercadolibre.data.model.response

import com.google.gson.annotations.SerializedName

data class ProductSearchResponse(
    val query: String,
    val paging: Pagination,
    val results: List<SearchItem>
)

data class Pagination(
    val total: Int,
    val offset: Int,
    val limit: Int,
)

data class SearchItem(
    val id: String,
    val title: String,
    val thumbnail: String,
    @SerializedName("available_quantity")
    val quantity: String,
    @SerializedName("prices")
    val pricesType: PricesType
)

data class PricesType(
    val id: String,
    val prices: List<Prices>
)

data class Prices(
    val id: String,
    val amount: Float,
    @SerializedName("currency_id")
    val currencyId: String
)
