package com.challenge.mercadolibre.com.challenge.mercadolibre.utils

import java.text.NumberFormat
import java.util.*

//region Float
fun Float.toCurrencyFormat(currencyCode: String = "COP"): String {
    val format: NumberFormat = NumberFormat.getCurrencyInstance()
    format.maximumFractionDigits = 0
    val currency = Currency.getInstance(currencyCode)
    return "${format.format(this)} ${currency.currencyCode}"
}
//endregion
