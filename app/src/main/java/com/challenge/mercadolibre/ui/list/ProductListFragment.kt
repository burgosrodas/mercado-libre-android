package com.challenge.mercadolibre.ui.list

import android.app.SearchManager
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import com.challenge.mercadolibre.R
import com.challenge.mercadolibre.common.base.BaseFragment
import com.challenge.mercadolibre.data.model.response.SearchItem
import com.challenge.mercadolibre.databinding.FragmentListBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductListFragment : BaseFragment() {

    override val viewModel: ProductViewModel by viewModels()
    override val binding by lazy {
        FragmentListBinding.inflate(layoutInflater)
    }

    private val adapter = ProductListAdapter(::onProductClicked)

    private var isSearchExpanded: Boolean = false
    private var searchQuery: String = ""

    private fun onProductClicked(searchItem: SearchItem) {
        findNavController().navigate(
            NavDeepLinkRequest.Builder.fromUri(
                Uri.parse(
                    getString(
                        R.string.detail_deep_link,
                        searchItem.id,
                        searchItem.title
                    )
                )
            ).build()
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isSearchExpanded = savedInstanceState?.getBoolean(SEARCH_EXPANDED_KEY, true) ?: true
        searchQuery = savedInstanceState?.getString(SEARCH_QUERY_KEY, "") ?: ""
        setHasOptionsMenu(true)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(SEARCH_EXPANDED_KEY, isSearchExpanded)
        outState.putString(SEARCH_QUERY_KEY, searchQuery)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_search, menu)
        configureSearchView(menu)
    }

    private fun configureSearchView(menu: Menu) {
        val searchItem: MenuItem? = menu.findItem(R.id.search)
        val searchManager = activity?.getSystemService(Context.SEARCH_SERVICE) as SearchManager

        (searchItem?.actionView as? SearchView)?.let { searchView ->

            searchView.setSearchableInfo(searchManager.getSearchableInfo(activity?.componentName))
            searchItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
                override fun onMenuItemActionExpand(menuItem: MenuItem): Boolean {
                    searchView.post {
                        searchView.setQuery(searchQuery, false)
                    }
                    isSearchExpanded = false
                    return true
                }

                override fun onMenuItemActionCollapse(menuItem: MenuItem?): Boolean {
                    isSearchExpanded = true
                    return true
                }
            })
            searchView.findViewById<ImageView>(R.id.search_close_btn)?.setOnClickListener {
                searchQuery = ""
                setInitState()
                searchView.setQuery("", false)
            }
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    if (!searchView.isIconified) {
                        searchView.isIconified = true
                    }
                    searchItem.collapseActionView()

                    if (!query.isNullOrEmpty()) {
                        viewModel.search(query = query.orEmpty())
                    } else {
                        searchQuery = ""
                        setInitState()
                    }
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    if (!newText.isNullOrEmpty()) {
                        searchQuery = newText
                    }
                    return true
                }
            })

            searchView.isIconified = isSearchExpanded
            searchView.setQuery(searchQuery, false)
            if (isSearchExpanded) {
                searchItem.collapseActionView()
            } else {
                searchItem.expandActionView()
            }
        }
    }

    override fun initData() = with(binding) {
        super.initData()
        viewModel.getEvents().observe(viewLifecycleOwner, ::handleEvents)
        viewModel.getQuery().observe(viewLifecycleOwner, ::updateSubtitle)
        binding.recyclerListView.let {
            it.layoutManager = GridLayoutManager(
                context,
                resources.getInteger(R.integer.grid_column_value)
            )
            it.itemAnimator = DefaultItemAnimator()
            it.adapter = adapter
            it.setHasFixedSize(false)
        }
    }

    private fun updateSubtitle(query: String) {
        setSubTitle(query)
    }

    override fun handleError(resId: Int) {
        displayMessage(
            resId,
            R.drawable.ic_error_outline
        )
    }

    /**
     * This will display a bunch of different messages, like error states, welcome messages,
     * empty results and so on, due the lack of time I didn't implement something more expressfull
     *
     * @param messageId String Resource id with the desired message to be displayed.
     * @param iconId Drawable Resource id with the Icon related to the message.
     * */
    private fun displayMessage(@StringRes messageId: Int, @DrawableRes iconId: Int) =
        with(binding) {
            recyclerListView.isVisible = false
            messageView.root.isVisible = true
            messageView.messageImage.setImageResource(iconId)
            messageView.messageDescription.text = resources.getString(messageId)
        }

    /**
     * Ideally the initial state would be a dashboard with some extra functionality
     * But now I only have a dumb message
     * */
    private fun setInitState() {
        updateSubtitle("")
        viewModel.cleanQuery()
        displayMessage(
            R.string.list_search_welcome_message,
            R.drawable.ic_outline_shopping_bag
        )
    }

    private fun setListItems(searchItems: List<SearchItem>) {
        adapter.setItems(searchItems)
        cleanMessage()
    }

    private fun cleanMessage() {
        binding.recyclerListView.isVisible = true
        binding.messageView.root.isVisible = false
    }

    private fun handleEvents(event: ProductEvents) {
        when (event) {
            is ProductEvents.SearchSuccess -> setListItems(event.searchItemsResult)
            is ProductEvents.Idle -> setInitState()
            ProductEvents.SearchEmptyResult -> displayMessage(
                R.string.list_search_empty_message,
                R.drawable.ic_product_placeholder
            )
            else -> Unit
        }
    }

    companion object {
        const val SEARCH_EXPANDED_KEY = "search_expanded"
        const val SEARCH_QUERY_KEY = "search_query"
    }
}
