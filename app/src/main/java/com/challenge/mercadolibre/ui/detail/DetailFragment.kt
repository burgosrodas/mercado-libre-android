package com.challenge.mercadolibre.ui.detail

import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.challenge.mercadolibre.R
import com.challenge.mercadolibre.com.challenge.mercadolibre.data.model.response.ProductDescriptionResponse
import com.challenge.mercadolibre.com.challenge.mercadolibre.utils.toCurrencyFormat
import com.challenge.mercadolibre.common.base.BaseFragment
import com.challenge.mercadolibre.data.model.response.ProductItemResponse
import com.challenge.mercadolibre.databinding.FragmentDetailBinding
import com.challenge.mercadolibre.ui.list.ProductEvents
import com.challenge.mercadolibre.ui.list.ProductViewModel
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailFragment : BaseFragment() {

    private val args: DetailFragmentArgs by navArgs()
    override val viewModel: ProductViewModel by viewModels()
    override val binding by lazy {
        FragmentDetailBinding.inflate(layoutInflater)
    }

    override fun initData() {
        super.initData()

        viewModel.getEvents().observe(viewLifecycleOwner, ::handleEvents)

        if (args.itemId.isNotEmpty()) {
            viewModel.getProductById(args.itemId)
            viewModel.getProductDescriptionById(args.itemId)
        }
    }

    private fun handleEvents(events: ProductEvents) {
        when (events) {
            ProductEvents.Idle -> Unit
            is ProductEvents.ProductSuccess -> setProductInfo(events.itemResult)
            is ProductEvents.ProductDescriptionSuccess -> setProductDescription(events.itemResult)
            else -> Unit
        }
    }

    private fun setProductDescription(productDescription: ProductDescriptionResponse) {
        binding.productDescription.text = productDescription.plainText
        binding.productDescription.isVisible = true
        binding.productDescriptionLabel.isVisible = true
    }

    private fun setProductInfo(product: ProductItemResponse) {
        with(binding) {

            productDescriptionPrice.text = product.price.toCurrencyFormat(product.currencyId)
            productDescriptionName.text = product.title

            Picasso.get()
                .load(product.pictures.firstOrNull()?.url ?: product.thumbnail)
                .placeholder(R.drawable.ic_image_placeholder)
                .error(R.drawable.ic_error_outline)
                .into(productDescriptionImage)
        }
    }
}
