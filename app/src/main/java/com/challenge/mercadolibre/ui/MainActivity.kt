package com.challenge.mercadolibre.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isInvisible
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.NavigationUI.setupActionBarWithNavController
import androidx.navigation.ui.navigateUp
import com.challenge.mercadolibre.R
import com.challenge.mercadolibre.common.UIEvents
import com.challenge.mercadolibre.databinding.ActivityMainBinding
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity: AppCompatActivity(), UIEvents {

    lateinit var binding: ActivityMainBinding

    private val navController by lazy { findNavController(R.id.nav_host_fragment) }
    private val appBarConfiguration by lazy {
        AppBarConfiguration(
            setOf(
                R.id.productsFragment
            ), null
        )
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun setSubTitle(subTitle: String?) {
        binding.mainToolbar.toolbar.subtitle = subTitle
    }

    override fun getSubTitle() = binding.mainToolbar.toolbar.subtitle?.toString().orEmpty()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.mainToolbar.toolbar)

        NavigationUI.setupWithNavController(
            binding.mainToolbar.toolbar,
            navController,
            appBarConfiguration
        )
        setupActionBarWithNavController(this, navController, appBarConfiguration)
    }

    override fun setLoading(isLoading: Boolean) {
        binding.mainToolbar.progressBar.isInvisible = !isLoading
    }

    override fun handleError(resId: Int) {
        Snackbar.make(binding.root, resId, Snackbar.LENGTH_LONG).show()
    }
}
