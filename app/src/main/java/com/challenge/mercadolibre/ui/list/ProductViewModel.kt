package com.challenge.mercadolibre.ui.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.challenge.mercadolibre.common.Result
import com.challenge.mercadolibre.common.base.BaseViewModel
import com.challenge.mercadolibre.data.repository.ProductRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductViewModel @Inject constructor(private val repository: ProductRepository) :
    BaseViewModel() {

    private val events = MutableLiveData<ProductEvents>(ProductEvents.Idle)
    private val currentQuery = MutableLiveData("")

    fun getEvents(): LiveData<ProductEvents> = events
    fun getQuery(): LiveData<String> = currentQuery

    fun cleanQuery() {
        currentQuery.value = ""
    }

    fun search(query: String) = viewModelScope.launch {
        currentQuery.value = query
        loading.value = true
        when (val result = repository.search(query)) {
            is Result.Success -> {
                if (result.data.results.isNotEmpty()) {
                    events.value = ProductEvents.SearchSuccess(result.data.results)
                } else {
                    events.value = ProductEvents.SearchEmptyResult
                }
                loading.value = false
            }
            is Result.Error -> {
                error.value = result.messageIdRes
                loading.value = false
            }
        }
    }

    fun getProductById(productId: String) = viewModelScope.launch {
        loading.value = true
        when (val result = repository.getProductById(productId)) {
            is Result.Success -> {
                events.value = ProductEvents.ProductSuccess(result.data)
                loading.value = false
            }
            is Result.Error -> {
                error.value = result.messageIdRes
                loading.value = false
            }
        }
    }

    fun getProductDescriptionById(productId: String) = viewModelScope.launch {
        loading.value = true
        when (val result = repository.getProductDescriptionById(productId)) {
            is Result.Success -> {
                events.value = ProductEvents.ProductDescriptionSuccess(result.data)
                loading.value = false
            }
            is Result.Error -> {
                error.value = result.messageIdRes
                loading.value = false
            }
        }
    }
}
