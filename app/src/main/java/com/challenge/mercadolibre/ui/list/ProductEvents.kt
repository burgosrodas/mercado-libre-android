package com.challenge.mercadolibre.ui.list

import com.challenge.mercadolibre.com.challenge.mercadolibre.data.model.response.ProductDescriptionResponse
import com.challenge.mercadolibre.data.model.response.ProductItemResponse
import com.challenge.mercadolibre.data.model.response.SearchItem

/*
 *   This sealed class contains all the possible events/states that can happen during the process
 *   and will be sent from the viewModel to the view to be processed
*/
sealed class ProductEvents {

    /**
     * This class represents the successful result for the search case.
     *
     * @param searchItemsResult List of products resulting of the search query.
     * */
    data class SearchSuccess(
        val searchItemsResult: List<SearchItem>
    ) : ProductEvents()

    /**
     * This class represents the successful result for the product requested by Id.
     * This is basically the case for the ProductDetail scenario
     * that needs only the info for a single Product.
     *
     * @param itemResult Product retrieved by id.
     * */
    data class ProductSuccess(
        val itemResult: ProductItemResponse
    ) : ProductEvents()

    /**
     * This class represents the successful result for the product description
     * that has their own API call.
     *
     * @param itemResult Product description.
     * */
    data class ProductDescriptionSuccess(
        val itemResult: ProductDescriptionResponse
    ) : ProductEvents()

    /**
     * This object represents the empty result of the search process.
     * */
    object SearchEmptyResult : ProductEvents()

    /**
     * This object represents the idle state.
     * */
    object Idle : ProductEvents()
}
