package com.challenge.mercadolibre.ui.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.challenge.mercadolibre.R
import com.challenge.mercadolibre.com.challenge.mercadolibre.utils.toCurrencyFormat
import com.challenge.mercadolibre.data.model.response.SearchItem
import com.challenge.mercadolibre.databinding.ItemProductBinding
import com.squareup.picasso.Picasso

class ProductListAdapter(private val onProductClicked: ((item: SearchItem) -> Unit)?) :
    RecyclerView.Adapter<ProductListAdapter.ViewHolder>() {

    val dataSet: ArrayList<SearchItem> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        dataSet[position].let {
            holder.bind(it, holder.item)
        }
    }

    fun setItems(items: List<SearchItem>) {
        val size = dataSet.size
        dataSet.clear()
        notifyItemRangeRemoved(0, size)
        dataSet.addAll(items)
        notifyItemRangeInserted(0, items.size)
    }

    inner class ViewHolder(val item: ItemProductBinding) : RecyclerView.ViewHolder(item.root) {

        fun bind(item: SearchItem, itemView: ItemProductBinding) {
            with(itemView) {
                root.setOnClickListener { onProductClicked?.invoke(item) }
                productName.text = item.title.trim()
                productQuantity.text = item.quantity

                item.pricesType.prices.firstOrNull()?.let {
                    productPrice.text = it.amount.toCurrencyFormat(it.currencyId)
                } ?: run {
                    productPrice.text = "-"
                }

                Picasso.get()
                    .load(item.thumbnail)
                    .placeholder(R.drawable.ic_image_placeholder)
                    .error(R.drawable.ic_error_outline)
                    .into(productImage)
            }
        }
    }

    override fun getItemCount() = dataSet.size
}
