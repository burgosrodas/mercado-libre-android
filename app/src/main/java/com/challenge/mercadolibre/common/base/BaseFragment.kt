package com.challenge.mercadolibre.common.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.challenge.mercadolibre.common.UIEvents

abstract class BaseFragment: Fragment(), UIEvents {

    abstract val binding: ViewBinding
    abstract val viewModel: BaseViewModel

    private val currentActivity: UIEvents by lazy {
        activity as UIEvents
    }

    open fun initData() {
        viewModel.loading().observe(viewLifecycleOwner, ::setLoading)
        viewModel.getError().observe(viewLifecycleOwner, ::handleError)
    }

    override fun setSubTitle(subTitle: String?) {
        (activity as? UIEvents)?.setSubTitle(subTitle = subTitle)
    }

    override fun getSubTitle() = (activity as? UIEvents)?.getSubTitle().orEmpty()

    override fun setLoading(isLoading: Boolean) = currentActivity.setLoading(isLoading)

    override fun handleError(@StringRes resId: Int) = currentActivity.handleError(resId)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = binding.root.also {
        initData()
    }
}
