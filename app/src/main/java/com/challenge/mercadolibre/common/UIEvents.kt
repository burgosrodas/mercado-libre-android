package com.challenge.mercadolibre.common

import androidx.annotation.StringRes

interface UIEvents {

    fun setLoading(isLoading: Boolean)

    fun handleError(@StringRes resId: Int)

    fun setSubTitle(subTitle: String?)

    fun getSubTitle(): String?
}
