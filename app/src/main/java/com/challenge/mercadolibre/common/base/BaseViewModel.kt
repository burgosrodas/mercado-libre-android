package com.challenge.mercadolibre.common.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseViewModel: ViewModel() {

    protected val error = MutableLiveData<Int>()
    protected val loading = MutableLiveData(false)

    fun getError(): LiveData<Int> = error
    fun loading(): LiveData<Boolean> = loading
}
