package com.challenge.mercadolibre.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

fun <T> LiveData<T>.awaitForValue(): T {
    val countDownLatch = CountDownLatch(1)
    var data: T? = null
    val observer = object : Observer<T> {
        override fun onChanged(o: T?) {
            data = o
            countDownLatch.countDown()
            this@awaitForValue.removeObserver(this)
        }
    }

    observeForever(observer)

    if (!countDownLatch.await(3, TimeUnit.SECONDS)) {
        throw TimeoutException("Time for waiting a response has been exceeded")
    }

    @Suppress("UNCHECKED_CAST")
    return data as T
}
