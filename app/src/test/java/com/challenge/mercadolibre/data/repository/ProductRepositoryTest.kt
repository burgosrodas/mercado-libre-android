package com.challenge.mercadolibre.data.repository

import com.challenge.mercadolibre.common.Result
import com.challenge.mercadolibre.data.networking.MercadoLibreAPI
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.IOException
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.InputStreamReader

/**
 * This will test the Repository class using a fake API using MockServer
 *
 * See [MercadoLibre API](https://developers.mercadolibre.com.ar/es_ar/items-y-busquedas).
 */
class ProductRepositoryTest {

    private lateinit var mockWebServer: MockWebServer
    private lateinit var repository: ProductRepository

    @Before
    @Throws(IOException::class)
    fun setUp() {
        mockWebServer = MockWebServer()
        mockWebServer.start()

        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(mockWebServer.url("").toString())
            .build()

        val service = retrofit.create(MercadoLibreAPI::class.java)

        repository = ProductRepository(service)
    }

    @Test
    fun testSearchWithEmptyQuery_ReturnsEmptyResult() = runBlocking {

        val emptyResponse = readFile("search_empty_response.json")

        mockWebServer.enqueue(MockResponse().setBody(emptyResponse))

        when (val response = repository.search("")) {
            is Result.Success -> {
                assertEquals(response.data.results.size, 0)
            }
            is Result.Error -> {
                assert(false)
            }
        }
    }

    @Test
    fun testSearchWithValidQuery_ReturnsNonZeroValues() = runBlocking {

        val successResponse = readFile("search_success_response.json")

        mockWebServer.enqueue(MockResponse().setBody(successResponse))

        when (val response = repository.search("Motorola G6")) {
            is Result.Success -> {
                assertNotEquals(response.data.results.size, 0)
            }
            is Result.Error -> {
                assert(false)
            }
        }
    }

    @Test
    fun testSearchWithError404_ReturnsClientError() = runBlocking {

        mockWebServer.enqueue(MockResponse().setResponseCode(404))

        when (val result = repository.search("Motorola G6")) {
            is Result.Success -> {
                assert(false)
            }
            is Result.Error -> {
                assertEquals(result.exception.message, "HTTP 404 Client Error")
            }
        }
    }

    @Test
    fun testProductById_ReturnsValidProductValue() = runBlocking {

        val successResponse = readFile("product_by_id_success_response.json")
        val productId = "MLA866888977"

        mockWebServer.enqueue(MockResponse().setBody(successResponse))

        when (val result = repository.getProductById(productId)) {
            is Result.Success -> {
                assertEquals(result.data.id, productId)
                assertEquals(result.success, true)
            }
            is Result.Error -> {
                assert(false)
            }
        }
    }

    @Test
    fun testProductByIdWithEmptyId_ReturnsError() = runBlocking {

        val errorResponse = readFile("product_by_id_error_response.json")

        mockWebServer.enqueue(MockResponse().setBody(errorResponse).setResponseCode(404))

        when (repository.getProductById("")) {
            is Result.Success -> {
                assert(false)
            }
            is Result.Error -> {
                assert(true)
            }
        }
    }

    private fun readFile(path: String): String {
        val reader = InputStreamReader(this.javaClass.classLoader?.getResourceAsStream(path))
        val content = reader.readText()
        reader.close()
        return content
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }
}
