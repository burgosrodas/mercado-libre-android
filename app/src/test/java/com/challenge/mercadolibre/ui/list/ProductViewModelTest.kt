package com.challenge.mercadolibre.ui.list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.challenge.mercadolibre.R
import com.challenge.mercadolibre.common.Result
import com.challenge.mercadolibre.data.model.response.Pagination
import com.challenge.mercadolibre.data.model.response.PricesType
import com.challenge.mercadolibre.data.model.response.ProductItemResponse
import com.challenge.mercadolibre.data.model.response.ProductSearchResponse
import com.challenge.mercadolibre.data.model.response.SearchItem
import com.challenge.mercadolibre.data.repository.ProductRepository
import com.challenge.mercadolibre.utils.awaitForValue
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class ProductViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @MockK
    private lateinit var repository: ProductRepository

    private lateinit var viewModel: ProductViewModel

    private val testDispatcher = TestCoroutineDispatcher()

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        Dispatchers.setMain(testDispatcher)
        viewModel = ProductViewModel(repository)
    }

    @Test
    fun testSearchForQuery_ReturnsError() = runBlockingTest {

        val query = ""

        coEvery {
            repository.search(query)
        } returns Result.Error(Throwable("Not found"), R.string.error_message_default)

        viewModel.search(query)

        Assert.assertTrue(viewModel.getError().awaitForValue() == R.string.error_message_default)
    }

    @Test
    fun testProductByIdForQuery_ReturnsSuccess() = runBlockingTest {

        val productId = "productId"

        coEvery {
            repository.getProductById(productId)
        } returns Result.Success(
            true, ProductItemResponse(
                id = productId,
                title = "",
                price = 0F,
                currencyId = "",
                quantity = "",
                condition = "",
                thumbnail = "",
                warranty = "",
                pictures = emptyList()
            )
        )

        viewModel.getProductById(productId)

        Assert.assertTrue(
            viewModel.getEvents().awaitForValue()
                    is ProductEvents.ProductSuccess
        )
    }

    @Test
    fun testSearchForEmptyQuery_ReturnsEmptyObject() = runBlockingTest {

        val query = ""

        coEvery {
            repository.search(query)
        } returns Result.Success(
            true, ProductSearchResponse(
                query,
                Pagination(limit = 0, total = 0, offset = 0),
                emptyList()
            )
        )

        viewModel.search(query)

        Assert.assertTrue(
            viewModel.getEvents().awaitForValue()
                    is ProductEvents.SearchEmptyResult
        )
    }

    @Test
    fun testSearchForValidQuery_ReturnsNonEmptyResponse() = runBlockingTest {

        val query = ""

        coEvery {
            repository.search(query)
        } returns Result.Success(
            true, ProductSearchResponse(
                query,
                Pagination(limit = 0, total = 1, offset = 0),
                listOf(
                    SearchItem(
                        id = "",
                        title = "",
                        thumbnail = "",
                        quantity = "",
                        PricesType(
                            id = "",
                            emptyList()
                        )
                    )
                )
            )
        )

        viewModel.search(query)

        val response = viewModel.getEvents().awaitForValue()

        Assert.assertTrue(response is ProductEvents.SearchSuccess)
        Assert.assertTrue((response as ProductEvents.SearchSuccess).searchItemsResult.isNotEmpty())
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }
}
